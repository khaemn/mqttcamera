import QtQuick 2.8
import QtQuick.Window 2.2
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1
import MqttClient 1.0

Item {
    id: root

    property MqttClient client

    property var tempSubscription: 0

    property alias hostname: hostnameField.text
    property alias port: portField.text
    property alias username: usernameField.text
    property alias password: passwordField.text
    property alias cameraTopicName: cameraSubField.text

    GridLayout {
        id: infoGrid

        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.margins: 20
        height: parent.height * 0.8

        columns: 2
        
        Label {
            text: "Hostname:"
            enabled: client.state === MqttClient.Disconnected
        }
        
        TextField {
            id: hostnameField
            Layout.fillWidth: true
            placeholderText: "<Enter host running MQTT broker>"
            selectByMouse: true
            enabled: client.state === MqttClient.Disconnected
        }
        
        Label {
            text: "Port:"
            enabled: client.state === MqttClient.Disconnected
        }
        
        TextField {
            id: portField
            Layout.fillWidth: true
            placeholderText: "<Port>"
            inputMethodHints: Qt.ImhDigitsOnly
            selectByMouse: true
            enabled: client.state === MqttClient.Disconnected
        }
        
        Label {
            text: "Username:"
            enabled: client.state === MqttClient.Disconnected
        }
        
        TextField {
            id: usernameField
            Layout.fillWidth: true
            placeholderText: "<Username>"
            inputMethodHints: Qt.ImhNone
            selectByMouse: true
            enabled: client.state === MqttClient.Disconnected
        }
        
        Label {
            text: "Password:"
            enabled: client.state === MqttClient.Disconnected
        }
        
        TextField {
            id: passwordField
            Layout.fillWidth: true
            placeholderText: "<Password>"
            inputMethodHints: Qt.ImhHiddenText
            echoMode: TextInput.Password
            selectByMouse: true
            enabled: client.state === MqttClient.Disconnected
        }
        
        Row {
            spacing: 50
            Layout.minimumHeight: 50
            Button {
                id: connectButton
                height: parent.height
                text: client.state === MqttClient.Disconnected
                      ? "Connect"
                      : "Disconnect"
                onClicked: {
                    if (client.state === MqttClient.Disconnected) {
                        client.hostname = root.hostname;
                        client.port = root.port;
                        client.username = root.username;
                        client.password = root.password;
                        client.saveSettings();
                        client.connectToHost();
                    } else {
                        client.disconnectFromHost();
                    }
                }
            }
        }
        
        RowLayout {
            enabled: client.state === MqttClient.Connected
            Layout.columnSpan: 2
            Layout.fillWidth: true
            
            Label {
                text: "Camera topic:"
            }
            
            TextField {
                id: cameraSubField
                text: "lab/roboarm/camera"
                placeholderText: "<Subscription topic>"
                selectByMouse: true
            }
            
            Button {
                id: cameraSubButton
                text: "Subscribe"
                onClicked: {
                    if (cameraSubField.text.length === 0)
                        return;
                    tempSubscription = client.subscribe(cameraSubField.text);
                }
            }
        }
    }

    Label {
        id: connectionStatus

        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20

        color: "#333333"
        text: "Status:" + client.connectionStatusString + "(" + client.state + ")"
        enabled: client.state === MqttClient.Connected
    }


    // TODO: this connection performs auto-subscribe, check if it is corect.
    Connections {
        target: root.client
        onStateChanged: {
            if (state === MqttClient.Connected) {
                cameraSubButton.clicked();
            }
        }
    }

    Component.onCompleted: {
        connectButton.clicked();
    }
}
