pragma Singleton

import QtQuick 2.8


Item {
    id: root

    property alias fontSizes: _fontSizes

    QtObject {
        id: _fontSizes

        readonly property int control: 22
        readonly property int small: 18
    }
}
