QT += qml quick mqtt

android {
    QT += androidextras
}

windows {
    #CONFIG += console
}

CONFIG += c++11

SOURCES += main.cpp \
    qmlmqttclient.cpp \
    simplecrypt/simplecrypt.cpp \
    androidhardware.cpp \
    mqttimageprocessor.cpp

HEADERS += \
    qmlmqttclient.h \
    simplecrypt/simplecrypt.hpp \
    androidhardware.h \
    mqttimageprocessor.h

RESOURCES += \
    qml.qrc

TARGET = mqttcamera

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

target.path = $$[QT_INSTALL_EXAMPLES]/mqtt/quicksubscription
INSTALLS += target

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat \
    android/AndroidManifest.xml

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

# Default rules for deployment.
#qnx: target.path = /tmp/$${TARGET}/bin
#else: unix:!android: target.path = /opt/$${TARGET}/bin
#!isEmpty(target.path): INSTALLS += target
