# RoboarmHMI

Mqtt Camera image sender/receiver

# Prerequisites:
* QT 5.11.3
* Android SDK 17c
* QtMqtt module built from https://code.qt.io/cgit/qt/qtmqtt.git/
