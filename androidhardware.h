#ifndef HAPTICFEEDBACK_H
#define HAPTICFEEDBACK_H

#include <QObject>

#ifdef ANDROID
#include <QtAndroidExtras>
#endif

class AndroidHardware : public QObject
{
    Q_OBJECT

public:
    explicit AndroidHardware(QObject *parent = nullptr);
    ~AndroidHardware();

public slots:
    Q_INVOKABLE void vibrate(int milliseconds);
    Q_INVOKABLE void keepScreenOn();

private:
#if defined(Q_OS_ANDROID)
    QAndroidJniObject m_wakeLock;
    QAndroidJniObject vibratorService;
#endif
};

#endif // HAPTICFEEDBACK_H
