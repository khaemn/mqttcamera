import QtQuick 2.8

Item {
    id: root

    property alias text: label.text

    anchors.fill: parent
    
    Rectangle {
        anchors.fill: parent
        color: "black"
        opacity: 0.5
    }
    Text {
        id: label
        anchors.centerIn: parent
        font.pixelSize: 40
        text: "No connection to the arm"
        color: "white"
    }
    MouseArea {
        id: mouseBlocker
        anchors.fill: parent
        onPositionChanged: {
            mouse.accepted = true;
        }
        onWheel: {
            wheel.accepted = true;
        }
    }
}
