import QtQuick 2.8
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.0
import QtMultimedia 5.7
import QtGraphicalEffects 1.0

import MqttClient 1.0
import MqttImageProcessor 1.0

Item {
    id: root

    property MqttClient client

    property bool isCameraAvailable: QtMultimedia.availableCameras.length > 0

    property var _locale: Qt.locale()
    property string _telemetry

    Camera {
        id: camera
    }

    Rectangle {
        id: grabSurface
        anchors.centerIn: parent

        // Width and height are fixed to max resolution to ensure image stability
        width: 1280
        height: 720
        visible: false

        VideoOutput {
            id: cameraView
            anchors.fill: parent
            source: camera
            focus : visible // to receive focus and capture key events when visible
            fillMode: VideoOutput.PreserveAspectFit
        }
        Column {
            id: telemetry
            anchors.left: cameraView.left
            anchors.leftMargin: cameraView.contentRect.x
            anchors.top: cameraView.top
            anchors.topMargin: cameraView.contentRect.y + 5

            Repeater {
                model: 8
                Text {
                    id: telemetryHeaderText
                    color: index %2 == 0 ? "white" : "black"
                    font.bold: true
                    font.pixelSize: cameraView.height / (10 * (index + 1))
                    text: _telemetry
                }
            }
        }
    }

    MqttImageProcessor {
        id: processor

        property int framesReceived: 0
        property real rxFps: 0
        property int framesSent: 0
        property real txFps: 0

        anchors.fill: parent
        client: root.client
        grabResolution: resolutions.currentIndex
        visible: !grabSurface.visible

        onReceivedImageChanged: {
            framesReceived++;
            if (rxFps < 5) {
                heartbeat.beat();
            }
        }
        onImageSent: {
            framesSent++;
        }
    }

    Text {
        id: rxFpsLabel
        text: "rx FPS: " + Math.round(processor.rxFps*100)/100
        color: "red"
        font.pixelSize: Style.fontSizes.control
    }

    Rectangle {
        id: heartbeat

        function beat() {
            heartbeatFadeout.start();
        }

        anchors.fill: processor

        opacity: 0.0
        color: "cyan"

        NumberAnimation {
            id: heartbeatFadeout
            target: heartbeat
            property: "opacity"
            duration: 100
            from: 0.2
            to: 0.0
        }
    }

    ColorOverlay {
        id: cameraPreview
        source: grabSurface
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 20
        width: parent.width / 4
        height: parent.height / 4
        visible: isCameraAvailable

        Text {
            id: txFpsLabel
            anchors.left: cameraPreview.left
            anchors.bottom: cameraPreview.bottom
            text: "tx FPS: " + Math.round(processor.txFps*100)/100
            color: "red"
            font.pixelSize: Style.fontSizes.control
        }
    }

    BackgroundDimmer {
        id: backgroundDimmer
        text: client.connectionStatusString
        visible: root.client.state !== MqttClient.Connected
    }

    Row {
        anchors.bottom: cameraPreview.bottom
        anchors.right: cameraPreview.left
        spacing: 5
        visible: isCameraAvailable
        Switch {
            id: isPictureProviderSwitch
            anchors.verticalCenter: shotPeriods.verticalCenter
            checked: processor.isPictureProvider
            onToggled: {
                processor.isPictureProvider = checked;
                if (checked) {
                    shotTimer.triggered();
                }
            }
        }
        Text {
            text: "Shot period, sec: "
            anchors.verticalCenter: shotPeriods.verticalCenter
            font.pixelSize: Style.fontSizes.small
        }
        ComboBox {
            id: shotPeriods
            model: [0.04, 0.05, 0.1, 0.2, 0.5, 1, 2, 5, 10]
            currentIndex: 6
            font.pixelSize: Style.fontSizes.small
        }

        ComboBox {
            id: resolutions
            model: ["128x72", "256x144", "384x216",
                    "512x288", "640x360", "768x432",
                    "896x504", "1024x576", "1280x720"]
            currentIndex: MqttImageProcessor.Resolution_256x144
            font.pixelSize: Style.fontSizes.small
        }
    }

    Rectangle {
        id: flasher

        function flash() {
            fadeout.start();
        }

        anchors.fill: cameraPreview
        opacity: 0.0

        NumberAnimation {
            id: fadeout
            target: flasher
            property: "opacity"
            duration: 100
            from: 0.4
            to: 0.0
        }
    }


    Timer {
        id: shotTimer
        interval: shotPeriods.currentText * 1000
        running: isCameraAvailable && processor.isPictureProvider
        repeat: true
        onTriggered: {
            var date = new Date();
            _telemetry = date.toLocaleString(_locale, "ddd yyyy-MM-dd hh:mm:ss");
            processor.grabSurface(grabSurface);
            // Flash animation is played only on low FPS rates.
            if (interval >= 200) {
                flasher.flash();
            }
        }
    }

    Timer {
        id: fpsMeasureTimer

        readonly property int seconds: 3
        readonly property int filter: 3

        interval: seconds * 1000
        running: true
        repeat: true
        onTriggered: {
            processor.rxFps = (processor.rxFps * (filter-1) + (processor.framesReceived / seconds) ) / filter;
            processor.txFps = (processor.txFps * (filter-1) + (processor.framesSent / seconds) ) / filter;
            processor.framesReceived = 0;
            processor.framesSent = 0;
        }
    }


    Connections {
        target: root.client
        onStateChanged: {
            if (state === MqttClient.Connected) {
                processor.subscribe();
            }
        }
    }
}
