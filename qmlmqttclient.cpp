/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qmlmqttclient.h"

#include <map>

#include <QDebug>
#include <QSettings>
#include <QtQml>

static constexpr auto S_HOSTNAME_STR = "hostname";
static constexpr auto S_PORT_STR = "port";
static constexpr auto S_USERNAME_STR = "username";
static constexpr auto S_PASSWORD_STR = "encrypted_password";

QmlMqttClient::QmlMqttClient(QObject *parent)
    : QMqttClient(parent)
    , m_connectionStatusString(tr("Press \"Connect\" to start"))
{
    connect(this,
            &QMqttClient::stateChanged,
            this,
            &QmlMqttClient::setConnectionStatusString
            );
    loadSettings();
}

QmlMqttClient::~QmlMqttClient()
{
    saveSettings();
}

void QmlMqttClient::registerQmlType()
{
    qmlRegisterType<QmlMqttClient>("MqttClient", 1, 0, "MqttClient");
    qmlRegisterUncreatableType<QmlMqttSubscription>("MqttClient", 1, 0, "MqttSubscription", QLatin1String("Subscriptions are read-only"));
}

QmlMqttSubscription* QmlMqttClient::subscribe(const QString &topic)
{
    qDebug() << "Subscribing to" << topic;
    auto sub = QMqttClient::subscribe(topic, 0);
    auto result = new QmlMqttSubscription(sub, this);
    return result;
}

qint32 QmlMqttClient::publishMessage(const QString &topic, const QByteArray &message)
{
    qDebug() << "Sending:" << topic << message.left(10) << " ... " << message.right(10);

    QMqttTopicName topicName(topic);
    return QMqttClient::publish(topicName, message);
}

QmlMqttSubscription::QmlMqttSubscription(QMqttSubscription *subscription, QmlMqttClient *client)
    : sub(subscription)
    , client(client)
{
    connect(sub, &QMqttSubscription::messageReceived, this, &QmlMqttSubscription::handleMessage);
    m_topic = sub->topic();
}

QString QmlMqttClient::connectionStatusString() const
{
    return m_connectionStatusString;
}

void QmlMqttClient::loadSettings()
{
    QSettings settings;
    settings.sync();

    setHostname(settings.value(S_HOSTNAME_STR,
                                                        "m21.cloudmqtt.com"
                               ).toString());
    setPort(static_cast<quint16>(settings.value(S_PORT_STR,
                                                        13309
                                 ).toUInt()));
    setUsername(settings.value(S_USERNAME_STR,
                                                        "nwqmqtvb"
                               ).toString());
    QString encryptedPassword =
            (settings.value(S_PASSWORD_STR,
                                                        ""
                           ).toString());

    QString decryptedPassword = "10FY76b4Mbkg"; // default pass
    if (!encryptedPassword.isEmpty()) {
        decryptedPassword = m_crypter.decryptToString(encryptedPassword);
    };
    setPassword(decryptedPassword);
}

void QmlMqttClient::saveSettings()
{
    QSettings settings;
    settings.setValue(S_HOSTNAME_STR, hostname());
    settings.setValue(S_PORT_STR, port());
    settings.setValue(S_USERNAME_STR, username());

    QString encryptedPassword = m_crypter.encryptToString(password());
    settings.setValue(S_PASSWORD_STR, encryptedPassword);
    settings.sync();
}

void QmlMqttClient::setConnectionStatusString(ClientState state)
{
    static const std::map<ClientState, QString> statusNames {
        {ClientState::Disconnected, tr("Disconnected")},
        {ClientState::Connecting, tr("Connecting...")},
        {ClientState::Connected, tr("Connected")}
    };

    auto statusName = statusNames.find(state);
    if (statusName == statusNames.end()) {
        m_connectionStatusString = tr("Unknown state");
    } else {
        m_connectionStatusString = statusName->second;
    }

    emit connectionStatusStringChanged(m_connectionStatusString);
}

void QmlMqttSubscription::handleMessage(const QMqttMessage &qmsg)
{
    emit messageReceived(qmsg.payload());
    emit messageReceivedRawData(qmsg.payload());
}

