#ifndef IMAGEPROCESSOR_H
#define IMAGEPROCESSOR_H

/* This class implements
 * 1. Grabbing a QQuickItem surface by demand
 *   A video frame from hardware camera is supposed to be shown on that surface.
 * 2. Encodes a grabbed image to JPEG string and sends to specified MQTT topic
 * 3. Listens to the same MQTT topic for a JPEG string
 * 4. Decodes the string to image and renders in on its own QQuickPaintedItem surface
 *   This class is a painted item and could be placed on a QML scene
 * TODO:    probably grabbing+encoding+sending and
 *          receiving+decoding+rendering might be splitted
 *          between 2 different classes.
 */

#include <memory>
#include <QQuickPaintedItem>
#include <QImage>
#include <QSharedPointer>
#include <QQuickImageProvider>
#include <QTimer>

#include "qmlmqttclient.h"
#include "simplecrypt/simplecrypt.hpp"

class QQuickItemGrabResult;

class MqttImageProcessor : public QQuickPaintedItem
{
    Q_OBJECT

    Q_PROPERTY(QmlMqttClient* client WRITE setMqttClient)
    Q_PROPERTY(QImage* receivedImage READ receivedImage NOTIFY receivedImageChanged)
    Q_PROPERTY(bool isPictureProvider READ isPictureProvider WRITE setIsPictureProvider NOTIFY isPictureProviderChanged)
    Q_PROPERTY(Resolution grabResolution READ grabResolution WRITE setGrabResolution NOTIFY grabResolutionChanged)

public:
    enum class Resolution : int {
        Resolution_128x72 = 0,
        Resolution_256x144,
        Resolution_384x216,
        Resolution_512x288,
        Resolution_640x360,
        Resolution_768x432,
        Resolution_896x504,
        Resolution_1024x576,
        Resolution_1280x720
    };
    Q_ENUM(Resolution)

    explicit MqttImageProcessor(QQuickPaintedItem *parent = nullptr);

    virtual void paint(QPainter *painter) override;

    static void registerQmlType();
    QImage* receivedImage() const;

    bool isPictureProvider() const;

    Resolution grabResolution() const;

    static constexpr int S_MAX_MQTT_FRAME_TIMEOUT_SEC = 10;
    static const std::map<Resolution, std::pair<int, int>> S_GRAB_RESOLUTIONS;

signals:
    void receivedImageChanged(QImage* receivedImage);
    void isPictureProviderChanged(bool isPictureProvider);
    void imageSent();
    void grabResolutionChanged(Resolution grabResolution);

public slots:
    Q_INVOKABLE void grabSurface(QQuickItem* s);
    Q_INVOKABLE void onMqttMessageReceived(const QByteArray& messagePayload);
    Q_INVOKABLE void setMqttClient(QmlMqttClient* sub);
    Q_INVOKABLE void subscribe();

    void processGrabResult();

    void setIsPictureProvider(bool isPictureProvider);
    void setGrabResolution(Resolution grabResolution);

private:
    Q_DISABLE_COPY(MqttImageProcessor)

    QSharedPointer<QQuickItemGrabResult> m_grab_result = nullptr;
    QmlMqttClient* m_client = nullptr;
    std::unique_ptr<QmlMqttSubscription> m_sub = nullptr;
    QQuickItem* m_surface = nullptr;
    std::unique_ptr<QImage> m_received_image;
    QTimer m_frame_timer;

    bool m_is_grabbing = false;
    bool m_was_new_image_received = false;
    bool m_is_picture_provider = false;
    bool m_was_frame_timed_out = false;
    Resolution m_grab_resolution = Resolution::Resolution_256x144;

    // 64-bit symmectric key could be any random of quint64 numbers.
    SimpleCrypt m_crypter { 0x0c24d4a4a0c4b1af };
};

#endif // IMAGEPROCESSOR_H
