#include "androidhardware.h"

// https://stackoverflow.com/questions/34179653/how-do-i-prevent-an-android-device-from-going-to-sleep-from-qt-application

#include <QDebug>

AndroidHardware::AndroidHardware(QObject *parent) : QObject(parent)
{
#if defined(Q_OS_ANDROID)
    QAndroidJniObject vibroString = QAndroidJniObject::fromString("vibrator");
    QAndroidJniObject activity = QAndroidJniObject::callStaticObjectMethod("org/qtproject/qt5/android/QtNative", "activity", "()Landroid/app/Activity;");
    QAndroidJniObject appctx = activity.callObjectMethod("getApplicationContext","()Landroid/content/Context;");
    vibratorService = appctx.callObjectMethod("getSystemService", "(Ljava/lang/String;)Ljava/lang/Object;", vibroString.object<jstring>());
#endif
}

AndroidHardware::~AndroidHardware()
{
#if defined(Q_OS_ANDROID)
    if ( m_wakeLock.isValid() )
    {
        m_wakeLock.callMethod<void>("release", "()V");
        qDebug() << "Unlocked device, can now go to standby";
    }
#endif
}

#if defined(Q_OS_ANDROID)

void AndroidHardware::vibrate(int milliseconds) {
    if (vibratorService.isValid()) {
        jlong ms = milliseconds;
        jboolean hasvibro = vibratorService.callMethod<jboolean>("hasVibrator", "()Z");
        vibratorService.callMethod<void>("vibrate", "(J)V", ms);
    } else {
        qDebug() << "No vibrator service available";
    }
}

void AndroidHardware::keepScreenOn()
{
    QAndroidJniObject activity = QAndroidJniObject::callStaticObjectMethod("org/qtproject/qt5/android/QtNative", "activity", "()Landroid/app/Activity;");
        if ( activity.isValid() )
        {
            QAndroidJniObject serviceName = QAndroidJniObject::getStaticObjectField<jstring>("android/content/Context","POWER_SERVICE");
            if ( serviceName.isValid() )
            {
                QAndroidJniObject powerMgr = activity.callObjectMethod("getSystemService", "(Ljava/lang/String;)Ljava/lang/Object;",serviceName.object<jobject>());
                if ( powerMgr.isValid() )
                {
                    jint levelAndFlags = QAndroidJniObject::getStaticField<jint>("android/os/PowerManager","SCREEN_DIM_WAKE_LOCK");

                    QAndroidJniObject tag = QAndroidJniObject::fromString( "My Tag" );

                    m_wakeLock = powerMgr.callObjectMethod("newWakeLock", "(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;", levelAndFlags,tag.object<jstring>());
                }
            }
        }

        if ( m_wakeLock.isValid() )
        {
            m_wakeLock.callMethod<void>("acquire", "()V");
            qDebug() << "Locked device, can't go to standby anymore";
        }
        else
        {
            assert( false );
        }
}
#else
void AndroidHardware::vibrate(int milliseconds) {
    Q_UNUSED(milliseconds);
}

void AndroidHardware::keepScreenOn() {

}

#endif
