#include "mqttimageprocessor.h"

#include <QtQml>
#include <QQuickItemGrabResult>
#include <QByteArray>
#include <QPainter>

// TEMP
#include <QDataStream>
#include <QBuffer>

#include "simplecrypt/simplecrypt.hpp"

static constexpr auto S_TOPIC = "lab/roboarm/camera";
static constexpr auto S_IMAGE_FORMAT = QImage::Format_Grayscale8;

// True-8 16:9 resolutions: 128x72, 256x144, 384x216, 512x288, 640x360, 768x432, 896x504, 1024x576, 1280x720
const std::map<MqttImageProcessor::Resolution,
               std::pair<int, int>>
    MqttImageProcessor::S_GRAB_RESOLUTIONS {
    { Resolution::Resolution_128x72,    {128,   72 } },
    { Resolution::Resolution_256x144,   {256,   144} },
    { Resolution::Resolution_384x216,   {384,   216} },
    { Resolution::Resolution_512x288,   {512,   288} },
    { Resolution::Resolution_640x360,   {640,   360} },
    { Resolution::Resolution_768x432,   {768,   432} },
    { Resolution::Resolution_896x504,   {896,   504} },
    { Resolution::Resolution_1024x576,  {1024,  576} },
    { Resolution::Resolution_1280x720,  {1280,  720} },
};

MqttImageProcessor::MqttImageProcessor(QQuickPaintedItem *parent)
    : QQuickPaintedItem(parent)
    , m_received_image(new QImage())
{
    setFlag(QQuickPaintedItem::ItemHasContents);

    m_frame_timer.setInterval(S_MAX_MQTT_FRAME_TIMEOUT_SEC * 1000);
    connect(&m_frame_timer,
            &QTimer::timeout,
            this,
            [=](){
                    m_was_frame_timed_out = true;
                    update();
                }
    );
    m_frame_timer.start();
}

void MqttImageProcessor::paint(QPainter *painter)
{
    if (m_received_image.get() == nullptr
            || m_received_image->isNull()
            || m_was_frame_timed_out) {
        painter->fillRect(contentsBoundingRect(), Qt::white);
    } else {
        QImage scaled =
                m_received_image->scaled(width(),
                                         height(),
                                         Qt::AspectRatioMode::KeepAspectRatio);
        painter->drawImage(0,0, scaled);
    }
}

void MqttImageProcessor::registerQmlType()
{
    qmlRegisterType<MqttImageProcessor>("MqttImageProcessor", 1, 0, "MqttImageProcessor");
}

QImage *MqttImageProcessor::receivedImage() const
{
    return m_received_image.get();
}

bool MqttImageProcessor::isPictureProvider() const
{
    return m_is_picture_provider;
}

MqttImageProcessor::Resolution MqttImageProcessor::grabResolution() const
{
    return m_grab_resolution;
}

void MqttImageProcessor::grabSurface(QQuickItem *s)
{
    if (m_is_grabbing || s == nullptr || !m_is_picture_provider) {
        return;
    }
    if (s != m_surface) {
        m_surface = s;
    }

    m_grab_result = m_surface->grabToImage();
    connect(m_grab_result.data(),
            &QQuickItemGrabResult::ready,
            this,
            &MqttImageProcessor::processGrabResult);

    m_is_grabbing = true;
}

void MqttImageProcessor::onMqttMessageReceived(const QByteArray& messagePayload)
{
    qDebug() << "Message received:" << messagePayload.left(10) << " ... " << messagePayload.right(10);

    QBuffer buffer(const_cast<QByteArray*>(&messagePayload));

    QByteArray decrypted = m_crypter.decryptToByteArray(buffer.data());

    m_received_image->loadFromData((uchar*)(decrypted.data()), decrypted.size(), "JPG");
    m_was_frame_timed_out = false;
    m_frame_timer.start();
    update();
    emit receivedImageChanged(m_received_image.get());
}

void MqttImageProcessor::setMqttClient(QmlMqttClient *client)
{
    if (client == nullptr || client == m_client) {
        return;
    }

    m_client = client;
}

void MqttImageProcessor::subscribe()
{
    if (!m_client) {
        return;
    }
    if (m_sub.get()) {
        disconnect(m_sub.get(),
                   &QmlMqttSubscription::messageReceivedRawData,
                   this,
                   &MqttImageProcessor::onMqttMessageReceived);
    }
    m_sub.reset(m_client->subscribe(S_TOPIC));
    connect(m_sub.get(),
            &QmlMqttSubscription::messageReceivedRawData,
            this,
            &MqttImageProcessor::onMqttMessageReceived);
}

void MqttImageProcessor::processGrabResult()
{
    disconnect(m_grab_result.data(),
                &QQuickItemGrabResult::ready,
                this,
                &MqttImageProcessor::processGrabResult);
    QImage img = m_grab_result->image().convertToFormat(S_IMAGE_FORMAT);
    m_is_grabbing = false;

    const auto resolution = S_GRAB_RESOLUTIONS.at(m_grab_resolution);
    img = img.scaled(resolution.first, resolution.second);

    QByteArray imageData = QByteArray::fromRawData(
                reinterpret_cast<const char*>(img.bits()),
                img.sizeInBytes());

    qDebug() << "\nGrabbed " << img.sizeInBytes() << "bytes of image";
    imageData = qCompress(imageData, 9);

    QBuffer jpegBuf;
    jpegBuf.open(QIODevice::WriteOnly);
    img.save(&jpegBuf, "JPG");

    QByteArray encrypted = m_crypter.encryptToByteArray(jpegBuf.data());

    qDebug() << "Compressed image size: " << encrypted.size();
    if (m_client) {
        m_client->publishMessage(S_TOPIC, encrypted);
        emit imageSent();
    }
}

void MqttImageProcessor::setIsPictureProvider(bool isPictureProvider)
{
    if (m_is_picture_provider == isPictureProvider)
        return;

    m_is_picture_provider = isPictureProvider;
    emit isPictureProviderChanged(m_is_picture_provider);
}

void MqttImageProcessor::setGrabResolution(MqttImageProcessor::Resolution grabResolution)
{
    if (m_grab_resolution == grabResolution)
        return;

    m_grab_resolution = grabResolution;
    emit grabResolutionChanged(m_grab_resolution);
}

